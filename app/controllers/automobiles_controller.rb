# frozen_string_literal: true

class AutomobilesController < ApplicationController
  def index
    @automobiles = Automobile.all
    render json: @automobiles
  end

  def show
    @automobile = Automobile.find(params[:id])
    render json: @automobile
  end

  def create
    @automobile = Automobile.create(param)
    render json: @automobile
  end

  def update
    @automobile = Automobile.find(params[:id])
    @automobile.update(param)
    render json: @automobile
  end

  def destroy
    @automobiles = Automobile.all
    @automobile = Automobile.find(params[:id])
    @automobile.destroy
    render json: @automobiles
  end

  def param
    params.permit(:car_brand, :car_model, :manufacturing_year, :horse_power)
  end
end
