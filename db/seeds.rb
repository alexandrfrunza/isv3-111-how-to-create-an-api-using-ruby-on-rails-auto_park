# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
Automobile.create(car_brand: 'BMW', car_model: '328ix', manufacturing_year: 2014, horse_power: 245)
Automobile.create(car_brand: 'BMW', car_model: 'X3', manufacturing_year: 2016, horse_power: 199)
Automobile.create(car_brand: 'VW', car_model: 'Golf GTI', manufacturing_year: 2017, horse_power: 225)
