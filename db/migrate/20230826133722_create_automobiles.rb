# frozen_string_literal: true

class CreateAutomobiles < ActiveRecord::Migration[7.0]
  def change
    create_table :automobiles do |t|
      t.string :car_brand
      t.string :car_model
      t.integer :manufacturing_year
      t.integer :horse_power
      t.timestamps
    end
  end
end
