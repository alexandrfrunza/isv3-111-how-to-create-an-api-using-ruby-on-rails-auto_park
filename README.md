# ISV3-111 How to create an API using Ruby on Rails Auto_park

API with list of cars.

## Installation

Download full archive from [gitlab](https://gitlab.com/alexandrfrunza/isv3-111-how-to-create-an-api-using-ruby-on-rails-auto_park/-/archive/main/isv3-111-how-to-create-an-api-using-ruby-on-rails-auto_park-main.zip) and unzip it to empty folder.

## How to run

Use your terminal and navigate to unziped folder.

```bash
cd path/to/unziped/folder
```

run docker-compose 

```bash
docker-compose up -d
```

run migrations

```bash
docker-compose exec app rails db:migrate
```

run your seeds

```bash
docker-compose exec app rails db:seed
```
## Using api


Find in unziped folder file:

```bash
automobiles.postman_collection.json
```

Import it in postman.
In that imported file you can find get/post/put/delete instance comands. 